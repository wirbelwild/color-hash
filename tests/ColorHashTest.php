<?php

/**
 * Bit&Black Color Hash
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ColorHash\Tests;

use BitAndBlack\ColorHash\ColorHash;
use Color\Value\Exception\InvalidInputNumberException;
use PHPUnit\Framework\TestCase;

class ColorHashTest extends TestCase
{
    public function testCanCreateHash(): void
    {
        $color = new ColorHash('A');
        
        self::assertSame(
            [
                'H' => 19,
                'S' => 65,
                'L' => 50,
            ],
            $color->getValues()
        );

        $color = new ColorHash('B');

        self::assertSame(
            [
                'H' => 150,
                'S' => 35,
                'L' => 65,
            ],
            $color->getValues()
        );
    }

    /**
     * @throws InvalidInputNumberException
     */
    public function testCanConvertColor(): void
    {
        $color = new ColorHash('A');

        self::assertSame(
            [
                'R' => 210,
                'G' => 97,
                'B' => 45,
            ],
            $color->getRGB()->getValues()
        );
    }
}
