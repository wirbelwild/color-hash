<?php

/**
 * Bit&Black Color Hash
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ColorHash;

use Color\Value\Exception\InvalidInputNumberException;
use Color\Value\HSL;

/**
 * @see \BitAndBlack\ColorHash\Tests\ColorHashTest
 */
class ColorHash extends HSL
{
    /**
     * @var array<int, int>
     */
    private array $S = [
        35,
        50,
        65,
    ];

    /**
     * @var array<int, int>
     */
    private array $L = [
        35,
        50,
        65,
    ];

    /**
     * ColorHash constructor.
     *
     * @param string $string
     * @throws InvalidInputNumberException
     */
    public function __construct(string $string)
    {
        $color = $this->getColorFromString($string);
        parent::__construct(...$color);
    }

    /**
     * @return int
     */
    private function getCharCode(string $character): int
    {
        $code = unpack('N', mb_convert_encoding($character, 'UCS-4BE', 'UTF-8'));
        
        if (false === $code) {
            return -1;
        }
        
        return $code[1] ?? -1;
    }

    /**
     * @return array<int, int>
     */
    private function getColorFromString(string $string): array
    {
        $seed = 131;
        $seed2 = 137;
        $hash = 0;
        
        /**
         * Make hash more sensitive for short string like 'a', 'b', 'c'
         */
        $string .= 'x';
        
        $max = 9_007_199_254_740_991 / $seed2;

        for ($counter = 0, $length = mb_strlen($string, 'UTF-8'); $counter < $length; ++$counter) {
            if ($hash > $max) {
                $hash /= $seed2;
            }

            $hash = $hash * $seed + $this->getCharCode(
                mb_substr($string, $counter, 1, 'UTF-8')
            );
        }
        
        $H = round($hash) % 359;
        $hash = (int) $hash / 360;
        
        $S = $this->S[(int) $hash % count($this->S)];
        $hash = (int) $hash / count($this->S);
        
        $L = $this->L[(int) $hash % count($this->L)];

        return [
            $H,
            $S,
            $L,
        ];
    }
}
