# Changes in Bit&Black Color Hash v2.0

## 2.0.0 2023-08-03

### Changed 

-   PHP >= 8.2 is now required.