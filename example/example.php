<?php

/**
 * Bit&Black Color Hash
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

use BitAndBlack\ColorHash\ColorHash;

require '../vendor/autoload.php';

$strings = [
    'test1',
    'test2',
    'test3',
];

$colors = [];

foreach ($strings as $string) {
    $colors[$string] = new ColorHash($string);
}

foreach ($colors as $color) {
    echo '<div style="width: 100px; height: 100px; background-color: rgb(' . $color->getRGB() . ');"></div>' . PHP_EOL;
}
