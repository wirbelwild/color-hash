[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/color-hash)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/9d68bf6a287f4bf0927bd822b24b9a20)](https://www.codacy.com/bb/wirbelwild/color-hash/dashboard)
[![Total Downloads](https://poser.pugx.org/bitandblack/color-hash/downloads)](https://packagist.org/packages/bitandblack/color-hash)
[![License](https://poser.pugx.org/bitandblack/color-hash/license)](https://packagist.org/packages/bitandblack/color-hash)

# Bit&Black Color Hash

Generates a color based on a given string. 

This library is based on [shahonseven/php-color-hash](https://packagist.org/packages/shahonseven/php-color-hash) which is a PHP port of [ColorHash Javascript Library](https://github.com/zenozeng/color-hash), but adds a lot more functions. 

## Installation

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/color-hash). Add it to your project by running `$ composer require bitandblack/color-hash`.

## Usage

```php
<?php 

use BitAndBlack\ColorHash\ColorHash;

$colorHash = new ColorHash('Hello World');
```

You can access the color values then in an array by calling `$colorHash->getValues()` or echo the whole object.

This library used [bitandblack/colors](https://packagist.org/packages/bitandblack/colors), so you can get the color information in many different color spaces. To get it for example in CMYK write `$colorHash->getCMYK()`. There are HEX, RGB, CIALab and many more available.

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
